<?php
$conn = new mysqli('localhost', 'client_f', 'W$^vAVkeE=*^-^LkyE2h*QQmQ', 'copa_del_saber');
$query_jornada1E = "SELECT @curRow := @curRow + 1 as posicion, 
                                    A.* ,
                                    A.puntosEntrenamiento + A.puntosCarrera_carrera as TOTAL
                                    FROM 
                                    
                                    (SELECT
                                        c.idCarrera AS idCarrera,
                                        u.idUsuario AS idUsuario,
                                        u.nombre AS nombreUsuario,
                                        equi.nombre AS nombreEquipo,
                                        c.idJornada AS idJornada,
                                     'Carrera' AS tipoCarrera,
                                        c.tiempo AS TiempoCarrera_carrera,
                                        SUM( DISTINCT c.puntos ) AS puntosCarrera_carrera,
                                        SUM( cp.acierto) AS preguntas_Correctas,
                                        per.nombre AS perfil,
                                        COUNT( cp.idPregunta ) AS preguntas_Realizadas_carrera,
                                        entrenamiento.preguntas_Realizadas AS preguntas_Realizadas_entrenamiento, 
                                        SUM( cp.acierto ) / COUNT( cp.idPregunta ) * 100 AS Asertividad, 
                                        entrenamiento.Asertividad AS Asertividad_entrenamiento, 
                                        c.tiempo * COUNT( cp.idPregunta ) AS tiempo_Real,
                                        entrenamiento.preguntas_Realizadas AS preguntasRealizadasEntrenamiento,
                                        entrenamiento.preguntas_Correctas AS preguntasCorrectasEntrenamiento,
                                        entrenamiento.Asertividad AS AsertividadEntrenamiento,
                                        entrenamiento.puntosCarrera AS puntosEntrenamiento
                                        
                                    FROM 
                                        `carreras` AS C
                                        INNER JOIN `carreras_preguntas` AS cp ON c.idCarrera = cp.idCarrera
                                        INNER JOIN usuarios AS u ON u.idUsuario = c.idUsuario
                                        INNER JOIN perfiles AS per ON u.idPerfil = per.idPerfil
                                        INNER JOIN equipos AS equi ON u.idEquipo = equi.idEquipo 
                                        LEFT JOIN (
                                        SELECT
                                        c.idCarrera AS idCarrera,
                                        u.idUsuario AS idUsuario,
                                        u.nombre AS nombreUsuario,
                                        equi.nombre AS nombreEquipo,
                                        c.idJornada AS idJornada,
                                     'Entrenamiento' AS tipoCarrera,
                                        c.tiempo AS TiempoCarrera,
                                        SUM( DISTINCT c.puntos ) AS puntosCarrera,
                                        SUM( cp.acierto) AS preguntas_Correctas,
                                        per.nombre AS perfil,
                                        COUNT( cp.idPregunta ) AS preguntas_Realizadas,
                                        SUM( cp.acierto ) / COUNT( cp.idPregunta ) * 100 AS Asertividad, 
                                        c.tiempo * COUNT( cp.idPregunta ) AS tiempo_Real
                                        
                                    FROM
                                        `carreras` AS C
                                        INNER JOIN `carreras_preguntas` AS cp ON c.idCarrera = cp.idCarrera
                                        INNER JOIN usuarios AS u ON u.idUsuario = c.idUsuario
                                        INNER JOIN perfiles AS per ON u.idPerfil = per.idPerfil
                                        INNER JOIN equipos AS equi ON u.idEquipo = equi.idEquipo 
                                        
                                        
                                    WHERE
                                        c.tipo = 2 
                                        AND c.idJornada = 1 
                                        AND per.nombre = 'Apoyo Comercial'
                                    GROUP BY
                                        u.idUsuario
                                    
                                     
                                        ) entrenamiento ON entrenamiento.idUsuario = u.idUsuario
                                        
                                    WHERE
                                        c.tipo = 1 
                                        AND c.idJornada = 1 
                                        AND per.nombre = 'Apoyo Comercial'
                                    GROUP BY
                                        u.idUsuario
                                    ORDER BY
                                        puntosCarrera_carrera,
                                        preguntas_Realizadas_carrera,
                                        TiempoCarrera_carrera 
                                        DESC
                                        ) A
                                         JOIN(SELECT @curRow := 0 ) r
                                        ORDER BY
                                        TOTAL DESC	
                                        
                                        
                                    ";
$result1 = $conn->query($query_jornada1E); ?>


<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Datos</title>
</head>
<body>
<div class="container d-flex justify-content-center">
    <div class="row d-flex justify-content-center">
        <div class="col-12">
            <table class="table  table-bordered">
                <thead>
                <tr>
                    <th rowspan="2" scope="col">posicion</th>
                    <th rowspan="2" scope="col">nombre</th>
                    <th rowspan="2" scope="col">equipo</th>
                    <th colspan="4" scope="row">Entrenamiento</th>
                    <th colspan="5" scope="row">Carrera</th>
                    <th colspan="1" rowspan="2" scope="row">Acumulado</th>
                </tr>
                <tr>
                    <th scope="col">Preguntas Realizadas</th>
                    <th scope="col">Preguntas Correctas</th>
                    <th scope="col">%Asertividad</th>
                    <th scope="col">Puntaje</th>
                    <th scope="col">Preguntas Realizadas</th>
                    <th scope="col">Preguntas Correctas</th>
                    <th scope="col">%Asertividad</th>
                    <th scope="col">Puntaje</th>
                    <th scope="col">Tiempo Carrera</th>
                </tr>
                </thead>

                <tbody>
                <?php while ($fila = $result1->fetch_assoc()) { ?>
                <tr>
                    <td> <?php echo($fila['posicion']) ?> </td>
                    <td> <?php echo($fila['nombreUsuario']) ?> </td>
                    <td> <?php echo($fila['nombreEquipo']) ?> </td>
                    <td> <?php echo($fila['preguntasRealizadasEntrenamiento']) ?> </td>
                    <td> <?php echo($fila['preguntasCorrectasEntrenamiento']) ?> </td>
                    <td> <?php echo($fila['AsertividadEntrenamiento']) ?> </td>
                    <td> <?php echo($fila['puntosEntrenamiento']) ?> </td>
                    <td> <?php echo($fila['preguntas_Realizadas_carrera']) ?> </td>
                    <td> <?php echo($fila['preguntas_Correctas']) ?> </td>
                    <td> <?php echo($fila['Asertividad']) ?> </td>
                    <td> <?php echo($fila['puntosCarrera_carrera']) ?> </td>
                    <td> <?php echo($fila['TiempoCarrera_carrera']) ?> </td>
                    <td> <?php echo($fila['TOTAL']) ?> </td>

                    <?php } ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
