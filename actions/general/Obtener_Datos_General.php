<?php
include('./database/conexionPDO.php');
$query = "SELECT @curRow := @curRow + 1 as posicion,
                    A.*
                     FROM 
                    (
                    SELECT 
                    nombre,
                    entrenamiento, 
                    carrera,
                    entrenamiento + carrera as total
                     FROM usuarios_jornada as uj
                    INNER JOIN usuarios as u ON u.idUsuario = uj.idUsuario
                    JOIN(SELECT @curRow := 0 ) r
                     
                    ) A
                     JOIN(SELECT @curRow := 0 ) r
                        ORDER BY
                        total DESC	
                        ";
$objconexion = new conexion();
$raking_general = $objconexion->consultar($query); ?>


<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Datos</title>
</head>
<body>
<div class="container d-flex justify-content-center">
    <div class="row d-flex justify-content-center">
        <div class="col-12">
            <table class="table  table-bordered">
                <thead>
                <tr>
                    <th rowspan="1" scope="col">posicion</th>
                    <th rowspan="1" scope="col">nombre</th>
                    <th rowspan="1" scope="col">equipo</th>
                    <th colspan="1" scope="row">Entrenamiento</th>
                    <th colspan="1" scope="row">Carrera</th>
                    <th colspan="1" rowspan="1" scope="row">Acumulado</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($raking_general

                as $fila) { ?>
                <tr>
                    <td> <?php echo($fila['posicion']) ?> </td>
                    <td> <?php echo($fila['nombre']) ?> </td>
                    <td> <?php echo($fila['entrenamiento']) ?> </td>
                    <td> <?php echo($fila['carrera']) ?> </td>
                    <td> <?php echo($fila['carrera']) ?> </td>
                    <td> <?php echo($fila['total']) ?> </td>

                    <?php } ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
