<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Copa del Saber</title>
</head>
<body>
<div class="row"
     style="align-items: stretch | flex-start | flex-end | center | baseline | first baseline | last baseline | start | end | self-start | self-end + ... safe | unsafe; ">
    <h4 class="d-flex justify-content-center ">Tabla de Raking</h4>
    <div class="card d-flex justify-content-center" style="">
        <div class="row">
            <div class="col-12 d-flex ">
                <div class="card-body ">
                    <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active btn btn-sm" id="pills-home-tab" data-bs-toggle="pill"
                                    data-bs-target="#jornada1"
                                    type="button" role="tab" aria-controls="pills-home" aria-selected="true">Jornada1
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link success " id="pills-profile-tab btn-sm" data-bs-toggle="pill"
                                    data-bs-target="#jornada2" type="button" role="tab" aria-controls="pills-profile"
                                    aria-selected="false">Jornada2
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button disabled class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                    data-bs-target="#jornada1" type="button" role="tab" aria-controls="pills-contact"
                                    aria-selected="false">
                                <a class="nav-link disabled">Jornada3</a>
                            </button>
                        </li>

                        <li class="nav-item" role="presentation">
                            <button disabled class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-contact" type="button" role="tab"
                                    aria-controls="pills-contact"
                                    aria-selected="false">
                                <a class="nav-link disabled">Jornada4</a>
                            </button>
                        </li>

                        <li class="nav-item" role="presentation">
                            <button disabled class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-contact" type="button" role="tab"
                                    aria-controls="pills-contact"
                                    aria-selected="false">
                                <a class="nav-link disabled">Todos Contra Todos</a>
                            </button>
                        </li>

                        <li class="nav-item" role="presentation">
                            <button disabled class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-contact" type="button" role="tab"
                                    aria-controls="pills-contact"
                                    aria-selected="false">
                                <a class="nav-link disabled">Semifinal</a>
                            </button>
                        </li>

                        <li class="nav-item" role="presentation">
                            <button disabled class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-contact" type="button" role="tab"
                                    aria-controls="pills-contact"
                                    aria-selected="false">
                                <a class="nav-link disabled">Final</a>
                            </button>
                        </li>

                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                    data-bs-target="#general-general" type="button" role="tab"
                                    aria-controls="pills-contact"
                                    aria-selected="false">General
                            </button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="jornada1" role="tabpanel"
                             aria-labelledby="pills-home-tab">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                                            data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                                            aria-selected="true">Cordinador
                                    </button>
                                    <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab"
                                            data-bs-target="#nav-profile" type="button" role="tab"
                                            aria-controls="nav-profile"
                                            aria-selected="false">Apoyo Comercial
                                    </button>
                                    <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab"
                                            data-bs-target="#nav-contact" type="button" role="tab"
                                            aria-controls="nav-contact"
                                            aria-selected="false">Ejecutivo
                                    </button>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                     aria-labelledby="nav-home-tab">

                                    <?php include('./actions/jornada1/obtener_datos_Coordinador.php'); ?>

                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                     aria-labelledby="nav-profile-tab">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                         aria-labelledby="nav-home-tab">
                                        <?php include('./actions/jornada1/obtener_datos_ApoyoComercial.php'); ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                     aria-labelledby="nav-contact-tab">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                         aria-labelledby="nav-home-tab">
                                        <?php include('./actions/jornada1/obtener_datos_Ejecutivo.php'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="jornada2" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                                            data-bs-target="#cordinadorj2" type="button" role="tab"
                                            aria-controls="nav-home"
                                            aria-selected="true">Cordinador
                                    </button>
                                    <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab"
                                            data-bs-target="#apoyoj2" type="button" role="tab"
                                            aria-controls="nav-profile"
                                            aria-selected="false">Apoyo Comercial
                                    </button>
                                    <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab"
                                            data-bs-target="#ejecutivoj22" type="button" role="tab"
                                            aria-controls="nav-contact"
                                            aria-selected="false">Ejecutivo
                                    </button>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="cordinadorj2" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                    <?php include('./actions/jornada2/obtener_datos_Coordinador.php'); ?>
                                </div>
                                <div class="tab-pane fade" id="apoyoj2" role="tabpanel"
                                     aria-labelledby="nav-profile-tab">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                         aria-labelledby="nav-home-tab">
                                        <?php include('./actions/jornada2/obtener_datos_ApoyoComercial.php'); ?>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="ejecutivoj22" role="tabpanel"
                                     aria-labelledby="nav-profile-tab">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                         aria-labelledby="nav-home-tab">
                                        <?php include('./actions/jornada2/obtener_datos_Ejecutivo.php'); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="general-general" role="tabpanel"
                             aria-labelledby="pills-contact-tab">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                                            data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                                            aria-selected="true">Datos Generales
                                    </button>

                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="general-general" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                    <div class="">
                                        <?php include('./actions/general/Obtener_Datos_General.php') ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"></script>
</body>
</html>
