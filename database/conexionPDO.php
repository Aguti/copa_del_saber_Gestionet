<?php

class conexion
{

    private $servername = "localhost";
    private $username = "client_f";
    private $password = "W$^vAVkeE=*^-^LkyE2h*QQmQ";
    private $conexion;

    public function __construct()
    {
        try {
            $this->conexion = new PDO("mysql:host=$this->servername;dbname=copa_del_saber", $this->username, $this->password);
            // set the PDO error mode to exception
            $this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            return "Conexion Fallida a la Base de Datos: " . $e->getMessage();
        }
    }

    public function consultar($sql)
    {
        $sentencia = $this->conexion->prepare($sql);
        $sentencia->execute();
        return $sentencia->fetchAll();
    }

}

?>