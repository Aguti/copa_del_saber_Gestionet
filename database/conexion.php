<?php

$query = "SELECT @curRow := @curRow + 1 as posicion, 
                                    A.* ,
                                    A.puntosEntrenamiento + A.puntosCarrera_carrera as TOTAL
                                    FROM 
                                    
                                    (SELECT
                                        c.idCarrera AS idCarrera,
                                        u.idUsuario AS idUsuario,
                                        u.nombre AS nombreUsuario,
                                        equi.nombre AS nombreEquipo,
                                        c.idJornada AS idJornada,
                                     'Carrera' AS tipoCarrera,
                                        c.tiempo AS TiempoCarrera_carrera,
                                        SUM( DISTINCT c.puntos ) AS puntosCarrera_carrera,
                                        SUM( cp.acierto) AS preguntas_Correctas,
                                        per.nombre AS perfil,
                                        COUNT( cp.idPregunta ) AS preguntas_Realizadas_carrera,
                                        entrenamiento.preguntas_Realizadas AS preguntas_Realizadas_entrenamiento, 
                                        SUM( cp.acierto ) / COUNT( cp.idPregunta ) * 100 AS Asertividad, 
                                        entrenamiento.Asertividad AS Asertividad_entrenamiento, 
                                        c.tiempo * COUNT( cp.idPregunta ) AS tiempo_Real,
                                        entrenamiento.preguntas_Realizadas AS preguntasRealizadasEntrenamiento,
                                        entrenamiento.preguntas_Correctas AS preguntasCorrectasEntrenamiento,
                                        entrenamiento.Asertividad AS AsertividadEntrenamiento,
                                        entrenamiento.puntosCarrera AS puntosEntrenamiento
                                        
                                    FROM 
                                        `carreras` AS C
                                        INNER JOIN `carreras_preguntas` AS cp ON c.idCarrera = cp.idCarrera
                                        INNER JOIN usuarios AS u ON u.idUsuario = c.idUsuario
                                        INNER JOIN perfiles AS per ON u.idPerfil = per.idPerfil
                                        INNER JOIN equipos AS equi ON u.idEquipo = equi.idEquipo 
                                        LEFT JOIN (
                                        SELECT
                                        c.idCarrera AS idCarrera,
                                        u.idUsuario AS idUsuario,
                                        u.nombre AS nombreUsuario,
                                        equi.nombre AS nombreEquipo,
                                        c.idJornada AS idJornada,
                                     'Entrenamiento' AS tipoCarrera,
                                        c.tiempo AS TiempoCarrera,
                                        SUM( DISTINCT c.puntos ) AS puntosCarrera,
                                        SUM( cp.acierto) AS preguntas_Correctas,
                                        per.nombre AS perfil,
                                        COUNT( cp.idPregunta ) AS preguntas_Realizadas,
                                        SUM( cp.acierto ) / COUNT( cp.idPregunta ) * 100 AS Asertividad, 
                                        c.tiempo * COUNT( cp.idPregunta ) AS tiempo_Real
                                        
                                    FROM
                                        `carreras` AS C
                                        INNER JOIN `carreras_preguntas` AS cp ON c.idCarrera = cp.idCarrera
                                        INNER JOIN usuarios AS u ON u.idUsuario = c.idUsuario
                                        INNER JOIN perfiles AS per ON u.idPerfil = per.idPerfil
                                        INNER JOIN equipos AS equi ON u.idEquipo = equi.idEquipo 
                                        
                                        
                                    WHERE
                                        c.tipo = 2 
                                        AND c.idJornada = 2 
                                        AND per.nombre = 'Coordinador'
                                    GROUP BY
                                        u.idUsuario
                                    
                                     
                                        ) entrenamiento ON entrenamiento.idUsuario = u.idUsuario
                                        
                                    WHERE
                                        c.tipo = 1 
                                        AND c.idJornada = 2 
                                        AND per.nombre = 'Coordinador'
                                    GROUP BY
                                        u.idUsuario
                                    ORDER BY
                                        puntosCarrera_carrera,
                                        preguntas_Realizadas_carrera,
                                        TiempoCarrera_carrera 
                                        DESC
                                        ) A
                                         JOIN(SELECT @curRow := 0 ) r
                                        ORDER BY
                                        TOTAL DESC";
$conn = new mysqli('localhost', 'client_f', 'W$^vAVkeE=*^-^LkyE2h*QQmQ', 'copa_del_saber');

if ($conn->connect_errno) {
    die("La conexion a la Base de Datos ha fallado");
}

