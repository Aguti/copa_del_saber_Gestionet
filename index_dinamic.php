<?php
include('database/conexion.php');
$tab_menu='';
$tabmenu='';
$tabmenu2='';
$tabconten='';
$count=0;
$queryTabs1 = 'SELECT * FROM jornadas';
$tabs1= $conn->query($queryTabs1);

while ($row = mysqli_fetch_array($tabs1))
{
    if($count == 0){

        $tabmenu .= '<button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                                    data-bs-target="#'.str_replace(' ','',$row['titulo']).'" type="button" role="tab" aria-controls="nav-home"
                                    aria-selected="true">'.$row['titulo'].'
                            </button>';


    }else{
        $tabmenu .= '<button class="nav-link " id="nav-home-tab" data-bs-toggle="tab"
                                    data-bs-target="#'.str_replace(' ','',$row['titulo']).'" type="button" role="tab" aria-controls="nav-home"
                                    aria-selected="true">'.$row['titulo'].'
                            </button>';


    }
        $count++;
}

$tabmenu.=' <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                        data-bs-target="#general-general" type="button" role="tab" aria-controls="pills-contact"
                                        aria-selected="false">General
                                </button>
                           ';

?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Ranking Users</title>
</head>
<body>
    <div class="container">
        <h2>Ranking Users</h2>
        <div class="row">
            <br>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <?php echo $tabmenu ?>

                </div>

                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="jornada1" role="tabpanel"
                         aria-labelledby="pills-home-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                                        data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                                        aria-selected="true">Cordinador
                                </button>
                                <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab"
                                        data-bs-target="#nav-profile" type="button" role="tab"
                                        aria-controls="nav-profile"
                                        aria-selected="false">Apoyo Comercial
                                </button>
                                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab"
                                        data-bs-target="#nav-contact" type="button" role="tab"
                                        aria-controls="nav-contact"
                                        aria-selected="false">Ejecutivo
                                </button>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                 aria-labelledby="nav-home-tab">

                                <?php include('./actions/jornada1/obtener_datos_Coordinador.php'); ?>

                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                 aria-labelledby="nav-profile-tab">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                    <?php include('./actions/jornada1/obtener_datos_ApoyoComercial.php'); ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                 aria-labelledby="nav-contact-tab">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                    <?php include('./actions/jornada1/obtener_datos_Ejecutivo.php'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="jornada2" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                                        data-bs-target="#cordinadorj2" type="button" role="tab" aria-controls="nav-home"
                                        aria-selected="true">Cordinador
                                </button>
                                <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab"
                                        data-bs-target="#apoyoj2" type="button" role="tab" aria-controls="nav-profile"
                                        aria-selected="false">Apoyo Comercial
                                </button>
                                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab"
                                        data-bs-target="#ejecutivoj22" type="button" role="tab"
                                        aria-controls="nav-contact"
                                        aria-selected="false">Ejecutivo
                                </button>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="cordinadorj2" role="tabpanel"
                                 aria-labelledby="nav-home-tab">
                                <?php include('./actions/jornada2/obtener_datos_Coordinador.php'); ?>
                            </div>
                            <div class="tab-pane fade" id="apoyoj2" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                    <?php include('./actions/jornada2/obtener_datos_ApoyoComercial.php'); ?>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="ejecutivoj22" role="tabpanel"
                                 aria-labelledby="nav-profile-tab">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                    <?php include('./actions/jornada2/obtener_datos_Ejecutivo.php'); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="general-general" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                                        data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                                        aria-selected="true">Datos Generales
                                </button>

                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="general-general" role="tabpanel"
                                 aria-labelledby="nav-home-tab">
                                <div class="">
                                    <?php include('./actions/general/Obtener_Datos_General.php')?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </nav>
        </div>

    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"></script>
</body>
</html>